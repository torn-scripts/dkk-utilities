var apiKey = localStorage.getItem("dkkutils_apikey");
if (!apiKey || apiKey === null || apiKey === "") setTimeout(apiPrompt, 100);

function apiPrompt() {
    var key = prompt("Please enter your API key: ");
    if (key !== null && key !== "") setAPI(key);
}

function setAPI(key) {
    apiKey = key;
    
    if (!key || key === null || key === "") localStorage.removeItem("dkkutils_apikey");
    else localStorage.setItem("dkkutils_apikey", key);
}

function sendAPIRequest(part, id, selections) {
    return new Promise((resolve, reject) => {
        GM_xmlhttpRequest({
            method: "GET",
            url: `https://api.torn.com/${part}/${id}?selections=${selections}&key=${apiKey}`,
            onreadystatechange: function (res) {
                if (res.readyState > 3 && res.status === 200) {
                    var json = JSON.parse(res.response);
                    resolve(json);
                    
                    if (json.error) {
                        var code = json.error.code;
                        if (code == 2) setAPI(null);
                    }
                }
            },
            onerror: function (err) {
                console.log(err);
                reject('XHR error.');
            }
        })
    }).catch(e => {
        console.log(e);
    });
}