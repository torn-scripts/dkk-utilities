var logging = {
    regular: true,
    debug: false
};
var prefix = {
    regular: "[DKK] ",
    debug: "[DKK DEBUG] "
};

function setDebug(debug) {
    logging.debug = debug;
}
function setPrefixEasy(name) {
    prefix.regular = "[" + name + "] ";
    prefix.debug = "[" + name + " DEBUG] ";
}
function setPrefix(regular, debug) {
    prefix.regular = regular;
    prefix.debug = debug;
}

function log(message, object) {
    if (!logging.regular) return;
    
    console.log(prefix.regular + message + (object ? " -->" : ""));
    if (object) console.log(object);
}

function debug(message, object) {
    if (!logging.debug) return;
    
    console.log(prefix.debug + message + (object ? " -->" : ""));
    if (object) console.log(object);
}

function xhrIntercept(callback) {
    let oldXHROpen = window.XMLHttpRequest.prototype.open;
    window.XMLHttpRequest.prototype.open = function() {
        this.addEventListener('readystatechange', function() {
            if (this.readyState > 3 && this.status == 200) {
                var page = this.responseURL.substring(this.responseURL.indexOf("torn.com/") + "torn.com/".length, this.responseURL.indexOf(".php"));

                var json, uri;
                if (isJsonString(this.response)) json = JSON.parse(this.response);
                else uri = getUrlParams(this.responseURL);
                
                callback(page, json, uri);
            }
        });

        return oldXHROpen.apply(this, arguments);
    }
}

function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

/**
 * JavaScript Get URL Parameter (https://www.kevinleary.net/javascript-get-url-parameters/)
 *
 * @param String prop The specific URL parameter you want to retreive the value for
 * @return String|Object If prop is provided a string value is returned, otherwise an object of all properties is returned
 */
function getUrlParams( url, prop ) {
    var params = {};
    var search = decodeURIComponent( ((url) ? url : window.location.href).slice( window.location.href.indexOf( '?' ) + 1 ) );
    var definitions = search.split( '&' );

    definitions.forEach( function( val, key ) {
        var parts = val.split( '=', 2 );
        params[ parts[ 0 ] ] = parts[ 1 ];
    } );

    return ( prop && prop in params ) ? params[ prop ] : params;
}

function getSpecialSearch() {
    let hash = window.location.hash;
    
    return hash.replace("#/", "?");
}

function observeMutations(root, selector, runOnce, callback, options, callbackRemoval) {
    if (!options) options = { childList: true };

    var ran = false;
    new MutationObserver(function(mutations, me) {
        var check = $(selector);

        if (check.length) {
            ran = true;
            callback(mutations);

            if (runOnce) me.disconnect();
        } else if (ran) {
            ran = false;
            if (callbackRemoval) callbackRemoval(mutations);
        }
    }).observe(root, options);
}

function replaceAll(str, text, replace) {
    while (contains(str, text)) {
        str = str.replace(text, replace);
    }
    return str;
}

function contains(str, text) {
    return str.indexOf(text) >= 0;
}

function stripHtml(html) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    var stripped = tmp.textContent || tmp.innerText || "";

    stripped = replaceAll(stripped, "\n", "");
    stripped = replaceAll(stripped, "\t", "");
    stripped = replaceAll(stripped, "  ", " ");

    return stripped;
}